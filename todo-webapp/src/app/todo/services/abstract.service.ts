import {HttpHeaders} from '@angular/common/http';

export abstract class AbstractService {

  protected getHeaders() {
    const headers = new HttpHeaders();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    return headers;
  }
}
