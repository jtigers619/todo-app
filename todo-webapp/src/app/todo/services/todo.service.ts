import { Injectable } from '@angular/core';
import {TodoItem} from "../interfaces/todo-item";
import {HttpClient, HttpHeaders, HttpResponse} from "@angular/common/http";
import {AbstractService} from "./abstract.service";
import {Observable} from "rxjs/Observable";
import 'rxjs/Rx';

@Injectable()
export class TodoService extends AbstractService {

  //private url = 'http://localhost:9090/toDoAPI/api/v1/todos';
  private url = 'api/v1/todos';

  constructor(private http: HttpClient) {
    super();
  }

  public listToDo(): Observable<TodoItem[]> {
    return this.http
                  .get<TodoItem[]>(this.url);
  }

  public createToDo (item: TodoItem): Observable<TodoItem> {
    return this.http
                  .post<TodoItem>(this.url, item, {headers: this.getHeaders()});
  }

  public updateToDo(item: TodoItem): Observable<TodoItem> {
    return this.http
                  .put<TodoItem>(this.url, item, {headers: this.getHeaders()});
  }

  public readTodo (itemId: number): Observable<TodoItem> {
    return this.http
                  .get<TodoItem>(this.url + '/' + itemId);
  }

  public deleteTodo (itemId: number) {
    const headers = new HttpHeaders();
    headers.append('Accept', 'text/plain');
    return this.http
                  .delete(this.url + '/' + itemId, {headers : headers});
  }

}
