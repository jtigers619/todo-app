import { TodoItem } from '../interfaces/todo-item';
import { Injectable } from '@angular/core';

@Injectable()
export class StaticTodoService {

  private todoList: TodoItem[] = [];

  public getTodoList() {
    return this.todoList;
  }

  public addItem(item: TodoItem){
    this.todoList.push(item);
  }

  public getItem(index: number): TodoItem {
    return this.todoList[index];
  }

  public setItem(item: TodoItem, index: number){
    this.todoList[index] = item;
  }

  public removeItem(index: number){
    this.todoList.splice(index, 1);
  }
}
