import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {TodoService} from '../services/todo.service';
import {TodoItem} from './../interfaces/todo-item';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {
  constructor(private todoService: TodoService) {}

  ngOnInit() {}

  @Input()
  todoList: TodoItem[] = [];

  @Output()
  onRemove: EventEmitter<number> = new EventEmitter<number>();

  @Output()
  onEdit: EventEmitter<TodoItem> = new EventEmitter<TodoItem>();

  @Output()
  onComplete: EventEmitter<TodoItem> = new EventEmitter<TodoItem>();

  public onRemoveTodo(id: number) {
    this.onRemove.emit(id);
  }

  public onEditTodo(todoItem: TodoItem) {
    this.onEdit.emit(todoItem);
  }

  public onCompleteTodo (todoItem: TodoItem) {
    this.onComplete.emit(todoItem);
  }
}
