import { Component, OnInit } from '@angular/core';
import {TodoService} from './services/todo.service';
import {TodoItem} from './interfaces/todo-item';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  constructor(private todoService: TodoService, private snackBar: MatSnackBar) { }

  public currentMode: String = 'Add';

  public todoItem: TodoItem = {
    id: null,
    title: '',
    description: '',
    complete: false
  };

  public todoList: TodoItem[];

  ngOnInit() {
    // Get todo list on init
    this.listToDo();
  }

  public listToDo() {
    this.todoService.listToDo()
      .subscribe((items) => this.todoList = items,
        (error) => console.log('error while listing.'),
        () => console.log('list fetched.'));
  }

  public onAddItem () {
    // create deep copy of the todoItem
    let item = Object.assign({}, this.todoItem);

    if (!item.title.trim()) {
      this.snackBar.open('Title is required', '', {duration: 2000});
      return;
    }

    if (!item.description.trim()) {
      this.snackBar.open('Description is required', '', {duration: 2000});
      return;
    }

    if (this.currentMode === 'Add') {
      // Add todo item to the todoLIst
      this.todoService.createToDo(item)
        .subscribe((todo) => item = todo,
          (error) => console.log('error while create.'),
          () => {
            console.log('Todo created.');
            this.listToDo();
      });

    }else {
      this.updateTodo(item);
    }

    this.reset();
  }

  private updateTodo (todoItem: TodoItem) {
     // Update todo item in todoList
      this.todoService.updateToDo(todoItem)
        .subscribe((todo) => todoItem = todo,
          (error) => console.log('error while create.'),
          () => {
            console.log('Todo updated.');
            this.listToDo();
          });
      this.listToDo();
  }

  public onRemove(itemId: number) {
    this.snackBar.open('Are you sure!!', 'Yes', {duration: 6000})
            .onAction()
            .subscribe(() => {
                console.log('The snack-bar action was triggered!');
                this.todoService.deleteTodo(itemId)
                  .subscribe((res) => console.log('res'),
                    (error) => {
                      console.log('Error occured while deleting todo with id : ' + itemId);
                    },
                    () => {
                      console.log('Todo deleted with id : ' + itemId);
                      this.listToDo();
                    });
              });
  }

  public onComplete (todoItem: TodoItem) {
    todoItem.complete = true;
    this.updateTodo(todoItem);
  }

  public reset() {
    // Reset component state
    this.currentMode = 'Add';
    // Reset todo item
    this.todoItem.title = '';
    this.todoItem.description = '';
  }

  public onEdit(item: TodoItem) {
    this.todoItem = Object.assign({}, item);
    this.currentMode = 'Update';
  }

}
