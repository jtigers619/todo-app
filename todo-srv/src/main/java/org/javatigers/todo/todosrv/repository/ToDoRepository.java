package org.javatigers.todo.todosrv.repository;

import org.javatigers.todo.todosrv.model.ToDo;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

/**
 * 
 * @author ad
 *
 */
public interface ToDoRepository extends ReactiveMongoRepository<ToDo, String> {}
