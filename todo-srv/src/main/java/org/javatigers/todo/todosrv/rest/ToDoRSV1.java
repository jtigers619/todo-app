package org.javatigers.todo.todosrv.rest;

import org.javatigers.todo.todosrv.model.ToDo;
import org.javatigers.todo.todosrv.repository.ToDoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * 
 * @author ad
 *
 */
//@CrossOrigin
//@RestController
//@RequestMapping(value = "api/v1/todos")
public class ToDoRSV1 {

	private final ToDoRepository toDoRepository;
	
	@Autowired
	public ToDoRSV1(ToDoRepository toDoRepository) {
		this.toDoRepository = toDoRepository;
	}
	
	@GetMapping(value = "/{id}")
	public Mono<ToDo> readToDo (@PathVariable("id") String id) {
		return toDoRepository.findById(id);
	}
	
	@GetMapping
	public Flux<ToDo> listToDo () {
		return toDoRepository.findAll();
	}
	
	@PostMapping
	public Mono<ToDo> createToDo (@RequestBody ToDo toDo) {
		return toDoRepository.save(toDo);
	}
	
	@PutMapping
	public Mono<ToDo> updateToDo (@RequestBody ToDo toDo) {
		return toDoRepository.save(toDo);
	}
	
	@DeleteMapping(value = "/{id}")
	public void deleteToDo (@PathVariable("id") String id) {
		toDoRepository.deleteById(id);
	}
	
	@GetMapping(value = "ping")
	public String ping () {
		return "<h1>Welcome to ToDo API!</h1>";
	}
	
}
