package org.javatigers.todo.todosrv;

import java.util.Arrays;
import java.util.UUID;

import org.javatigers.todo.todosrv.model.ToDo;
import org.javatigers.todo.todosrv.repository.ToDoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.embedded.EmbeddedMongoAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.config.AbstractReactiveMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;

import lombok.RequiredArgsConstructor;

@EnableReactiveMongoRepositories
@SpringBootApplication(exclude = {MongoAutoConfiguration.class, MongoDataAutoConfiguration.class})
@AutoConfigureAfter(EmbeddedMongoAutoConfiguration.class)
@RequiredArgsConstructor
public class TodoSrvApplication extends AbstractReactiveMongoConfiguration {
	
	private static final Logger logger = LoggerFactory.getLogger(TodoSrvApplication.class);

	private final Environment environment;
	
	public static void main(String[] args) {
		SpringApplication.run(TodoSrvApplication.class, args);
	}
	
 
    @Override
    protected String getDatabaseName() {
        return "reactive-mongo";
    }
	@Bean
	public CommandLineRunner commandLineRunner(ToDoRepository toDoRepository) {
		return strings -> {
			toDoRepository.deleteAll().subscribe();
			Arrays.asList("Task1,Task1 Description", "Task2,Task2 Description", "Task3,Task3 Description", "Task4,Task4 Description")
				.stream()
				.map(str -> str.split(","))
				.map(tuple -> ToDo.builder().id(UUID.randomUUID().toString()).title(tuple[0]).description(tuple[1]).complete(false).build())
				.map(toDoRepository::save)
				.forEach(mono -> mono.subscribe(todo -> logger.info(todo.toString())));
		};
	}


	@Override
    @Bean
    @DependsOn("embeddedMongoServer")
	public MongoClient reactiveMongoClient() {
		int port = environment.getProperty("local.mongo.port", Integer.class);
		System.out.println("Mongo DB Port : " + port);
        return MongoClients.create(String.format("mongodb://localhost:%d", port));
	}
}