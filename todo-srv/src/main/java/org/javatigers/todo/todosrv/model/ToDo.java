package org.javatigers.todo.todosrv.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author ad
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Document
public class ToDo {
	
	@Id
	private String id;
	private String title;
	private String description;
	private boolean complete;
}
