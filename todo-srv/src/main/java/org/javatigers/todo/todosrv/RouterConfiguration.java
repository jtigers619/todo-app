package org.javatigers.todo.todosrv;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.server.RequestPredicates.DELETE;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;
import static org.springframework.web.reactive.function.server.RequestPredicates.contentType;
import static org.springframework.web.reactive.function.server.RequestPredicates.method;
import static org.springframework.web.reactive.function.server.RequestPredicates.path;
import static org.springframework.web.reactive.function.server.RouterFunctions.nest;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

import org.javatigers.todo.todosrv.model.ToDo;
import org.javatigers.todo.todosrv.repository.ToDoRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.config.EnableWebFlux;
import org.springframework.web.reactive.config.ResourceHandlerRegistry;
import org.springframework.web.reactive.config.WebFluxConfigurer;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.reactive.function.server.ServerResponse.BodyBuilder;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;

import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

/**
 * 
 * @author ad
 *
 */
@EnableWebFlux
@Configuration
public class RouterConfiguration implements WebFluxConfigurer {

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/**").addResourceLocations(CLASSPATH_RESOURCE_LOCATIONS);
	}
	
	private static final String[] CLASSPATH_RESOURCE_LOCATIONS = { "classpath:/META-INF/resources/" };
	
	@Bean
	public RouterFunction<?> routes (ToDoHandler toDoHandler) {
		return nest(path("/api/v1"), 
					nest(path("/todos"), 
							nest(accept(MediaType.APPLICATION_JSON), 
									route(GET("/{id}"), toDoHandler::readToDo)
										.andRoute(method(HttpMethod.GET), toDoHandler::listToDo)
										.andRoute(method(HttpMethod.POST).and(contentType(APPLICATION_JSON)), toDoHandler::createToDo)
										.andRoute(method(HttpMethod.PUT).and(contentType(APPLICATION_JSON)), toDoHandler::updateToDo)
										.andRoute(DELETE("/{id}"), toDoHandler::deleteToDo)))
					.andRoute(GET("/ping"), request -> 
							ServerResponse.ok().body(Mono.<String>just("ToDo API"), String.class)));
	}
	
	
}

@Component
@RequiredArgsConstructor
class ToDoHandler {

	private final ToDoRepository toDoRepository;
	
	public Mono<ServerResponse> readToDo (ServerRequest request) {
		return toDoRepository.findById(request.pathVariable("id"))
					.flatMap(todo -> getSrvBodyBuilder ().body(Mono.just(todo), ToDo.class))
					.switchIfEmpty(notFound);
	}
	
	public Mono<ServerResponse> listToDo (ServerRequest request) {
		return getSrvBodyBuilder ()
					.body(toDoRepository.findAll(), ToDo.class);
	}
	
	public Mono<ServerResponse> deleteToDo (ServerRequest request) {
		return toDoRepository
				.findById(request.pathVariable("id"))
				.doOnSuccess(toDo ->
						toDoRepository.deleteById(request.pathVariable("id"))
						.subscribe()
				)
				.flatMap(toDo -> getSrvBodyBuilder ().body(Mono.just(toDo), ToDo.class))
				.switchIfEmpty(notFound);
	}
	
	public Mono<ServerResponse> createToDo (ServerRequest request) {
		return request.bodyToMono(ToDo.class)
				.map(toDoRepository::save)
				.flatMap(toDo -> getSrvBodyBuilder ().body(toDo, ToDo.class));
	}
	
	public Mono<ServerResponse> updateToDo (ServerRequest request) {
		return request.bodyToMono(ToDo.class)
				      .filter(todo -> todo.getId() != null)
					  .map(toDoRepository::save)
					  .flatMap(toDo -> getSrvBodyBuilder ().body(toDo, ToDo.class))
					  .switchIfEmpty(notFound);
	}
	
	private BodyBuilder getSrvBodyBuilder () {
		return ServerResponse.ok()
					.contentType(MediaType.APPLICATION_JSON);
	}
	
	private Mono<ServerResponse> notFound = ServerResponse.notFound().build();
}

@Component
class CorsFilter implements WebFilter {

	@Override
	public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
		if (exchange != null) {
			exchange.getResponse().getHeaders().add("Access-Control-Allow-Origin", "*");
			exchange.getResponse().getHeaders().add("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
			exchange.getResponse().getHeaders().add("Access-Control-Allow-Headers", "DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range, X-XSRF-TOKEN");
            if (exchange.getRequest().getMethod() == HttpMethod.OPTIONS) {
            	exchange.getResponse().getHeaders().add("Access-Control-Max-Age", "1728000");
            	exchange.getResponse().setStatusCode(HttpStatus.NO_CONTENT);
                return Mono.empty();
            }
		}
		exchange.getResponse().getHeaders().add("Access-Control-Expose-Headers", "DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Content-Range,Range");
		return chain != null ? chain.filter(exchange) : Mono.empty();
	}
	
}

