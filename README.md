# TodoApp

#Integrate Angular in Spring Boot with Gradle

A demo app to demonstrate the crud opration using frontend as angular 4 and beckend as spring boot rest jpa h2

This application demonstrate the complete Angular app from Spring Boot. This TodoApp shows couple of simple steps to get everything up and running: run NPM from Gradle, integrate the Gradle frontend build in the main build and support HTML5 mode in the ResourceHandler of Spring Boot.

## Run NPM from Gradle
Build frontend code using gradle build on our project. We can use the plugin gradle-node-plugin for this. Following file contains gradle changes required to build frontend using gradle /todo-webapp/build.gradle file.

```bash

plugins {
  id "com.moowork.node" version "0.12"
}
 
version '0.0.1'
 
node {
  version = '6.8.0'
  npmVersion = '3.10.8'
  download = true
  workDir = file("${project.buildDir}/node")
  nodeModulesDir = file("${project.projectDir}")
}
 
task build(type: NpmTask) {
  args = ['run', 'build']
}
 
build.dependsOn(npm_install)

```

## Support Angular HTML5 mode
Now all we need to do is create support for HTML5 mode in Angular. Angular is a single page application and subroutes of the application are default served with a ‘#’ hashtag separator. If we want to have regular paths, we can enable HTML5 mode. The problem in serving this Angular HTML5 application from Spring Boot is that the Spring Boot ResourceHandler cannot find these resources, since the real resources is the index.html with the JavaScript files. With the next code snippet we instruct Spring Boot to look for the index.html as well. This is inspired by 

Node gradle plugin will copy frontend resources in META-INF/resources

```gradle

jar {
  dependsOn("npmBuild")
  from(fileTree("dist")) {
    into "META-INF/resources"
  }
}

```

And that jar is included as dependency in spring boot application.

```java
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/**").addResourceLocations(CLASSPATH_RESOURCE_LOCATIONS);
	}
	
	private static final String[] CLASSPATH_RESOURCE_LOCATIONS = { "classpath:/META-INF/resources/" };

```

## Development server

Run

```bash
	gradlew clean build
	gradlew bootRun
```
Run

```bash
	npm install
	ng serve
```
Navigate to: `http://localhost:4200/`.

###Update Angular Application

Global package:

```bash
	npm uninstall -g @angular/cli
	npm cache clean
	# if npm version is > 5 then use `npm cache verify` to avoid errors (or to avoid using --force) 
	npm install -g @angular/cli@latest
```

Local Upgrade

```bash
	npm install @angular/{common,compiler,compiler-cli,core,forms,http,platform-browser,platform-browser-dynamic,platform-server,router				,animations}@latest typescript@latest rxjs@latest --save
```

Update local project version, because inside your project directory it will be selected with higher priority than the global one:

Local project package:

```bash
	rm -rf node_modules
	npm uninstall --save-dev @angular/cli
	npm install --save-dev @angular/cli@latest
	npm install
```